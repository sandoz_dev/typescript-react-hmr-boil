import * as React from 'react';
import * as styles from './app.css';
import { fa, faQuestion } from './icons';

export default class Application extends React.Component<{}, {}> {
    render() {
        return <div>
            <h1 className={styles.highlight}><span className={[fa, faQuestion].join(' ')}/> HMR is go. 1, 2, 3, 4, 5!</h1>
        </div>
    }
}