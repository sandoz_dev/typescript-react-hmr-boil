import * as fontAwesome from 'font-awesome/css/font-awesome.css';

const { fa, faQuestion } = fontAwesome;

export {
    fa,
    faQuestion
}