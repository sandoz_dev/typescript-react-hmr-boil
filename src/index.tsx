import AppContainer from './app-container';
import * as React from 'react';
import * as ReactDOM from 'react-dom';

import App from './application';

const render = (Component) => {
    ReactDOM.render(
        <AppContainer>
            <Component/>
        </AppContainer>,
        document.getElementById('base-element')
    );
};

render(App);

// Hot Module Replacement API
if (module.hot) {
    module.hot.accept('./application', () => {
        render(require('./application').default);
    });
}