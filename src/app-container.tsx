import * as rhl from 'react-hot-loader';
// AppContainer is a necessary wrapper component for HMR
const { AppContainer } = rhl;

export default AppContainer;